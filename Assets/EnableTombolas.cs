using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTombolas : MonoBehaviour
{
    public GameObject tombolas;
    
    public void EnableTombola()
    {
        foreach (Transform go in tombolas.transform)
        {
            go.transform.gameObject.SetActive(true);            
        }    
    }
}
