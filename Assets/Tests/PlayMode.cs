/*
 * This was coded up this morning ans serves as a quick example of how easy and relatively quick writing regression tests can be.
 * Please forgive any bugs and the general slapdash code. 
 */


using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class TombolaMenuTests
    {
        [SetUp]
        public void BeforeEveryTest()
        {
            SceneManager.LoadScene("Assets/Scenes/Tombola.unity");
        }


        [UnityTest]
        public IEnumerator _00_Event_System_Exists_In_Main_Menu()
        {
            yield return null;
            Assert.NotNull(Object.FindObjectOfType<EventSystem>(), "Event System Object Not Found");
        }


        [UnityTest]
        public IEnumerator _01_Camera_Exists_In_Main_Menu()
        {
            yield return null;
            Assert.NotNull(Camera.main, "A MainCamera us missing from the scene");
        }
        
        
        [UnityTest]
        public IEnumerator _02_Camera_Exists_In_Main_Menu()
        {
            yield return null;
            Assert.NotNull(Camera.main, "A MainCamera us missing from the scene");
        }
        
        [UnityTest]
        public IEnumerator _03_Tombolas_Are_Disabled_On_Start()
        {
            yield return null;
            GameObject tombolaParent = GameObject.Find("Tombolas");

            foreach (Transform go in tombolaParent.transform)
            {
                Assert.IsFalse(go.gameObject.activeInHierarchy, "Gameobjects are not active in hierarchy");
            }
        }

        [UnityTest]
        public IEnumerator _04_Number_Of_Tombolas_GameObjects_Equals_Three()
        {
            yield return null;
            GameObject animatedBar = GameObject.Find("OrangeBar");
            GameObject tombolas = GameObject.Find("Tombolas");
            
            // Check we have that the orange bar is enabled. 
            Assert.IsNotNull(animatedBar);
            
            // Grab the animator component
            Animator animation = animatedBar.GetComponent<Animator>();
            
            // Yield and continue to move to the next frame until the the orange bar animation has finished playing. This enables the tombolas.
            yield return new WaitWhile(() => animation.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);

            // Now we can check the right amount appear in our test. In this case three. 
            Assert.AreEqual(3, tombolas.transform.childCount, "The number of expected tombolas is incorrect");
        }


        [UnityTest]
        public IEnumerator _05_Tombolas_Have_Rotation_Script()
        {
            yield return null;
            GameObject tombolas = GameObject.Find("Tombolas");
            GameObject animatedBar = GameObject.Find("OrangeBar");
            
            // Grab the animator component
            Animator animation = animatedBar.GetComponent<Animator>();
            
            // Yield and continue to move to the next frame until the the orange bar animation has finished playing. This enables the tombolas.
            yield return new WaitWhile(() => animation.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);
            
            // Check each tombola has a tombola component
            foreach (Transform t in tombolas.transform)
            {
                Assert.IsNotNull(t.GetComponent<RotateTumbola>(), "Gameobject is missing RotateTumbola component");
            }
        }


        [UnityTest]
        public IEnumerator _06_Tombolas_Have_Data_Script()
        {
            yield return null;
            GameObject tombolas = GameObject.Find("Tombolas");
            
            // Check each tombola has a tombola component
            foreach (Transform t in tombolas.transform)
            {
                Assert.IsNotNull(t.GetComponent<TumbolaData>(), "Gameobject is missing data component");
            }
        }
    }

    public class TombolaAnimationTests
    {
        [SetUp]
        public void BeforeEveryTest()
        {
            SceneManager.LoadScene("Assets/Scenes/Tombola.unity");
        }

        [UnityTest]
        public IEnumerator _00_Tombola_Has_Animator_And_Tombola_Controller()
        {
            yield return null;
            GameObject tombolaParent = GameObject.Find("Tombolas");

            foreach (Transform tom in tombolaParent.transform)
            {
                Animator currentTom = tom.GetComponent<Animator>();
                Assert.IsNotNull(currentTom);

                Assert.AreEqual("Tombola", currentTom.runtimeAnimatorController.name,
                    "Expected Tombola Animation Controller");
            }
        }

        [UnityTest]
        public IEnumerator _01_On_Click_Tombola_Animation_Plays()
        {
            yield return null;
            GameObject tombolas = GameObject.Find("Tombolas");

            // I should check if the orange bar animation is finished, then the tombola animation is finished before checking the invoke
            // but for this example adding a wait is fine.  
            yield return new WaitForSeconds(5);

            // Check that when i invoke a click the animation plays. I can check in two ways
            foreach (Transform t in tombolas.transform)
            {
                Animator animation = t.GetComponent<Animator>();
                Button button = t.GetComponentInChildren<Button>();
                Assert.IsNotNull(button);
                yield return null;

                button.onClick.Invoke();
                yield return null;
              
                // Now that i have invoked the button the tombola should have rotated.
                Assert.IsTrue(animation.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);

            }
        }
    }
}


