using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAll : MonoBehaviour
{
    public GameObject tombolas;

    public void OnClickRotateAllTombolas()
    {
        Animator logoAnimation = transform.GetChild(0).GetComponent<Animator>();
        logoAnimation.SetTrigger("RotateAnimation");
        
        StartCoroutine(Wait());
    }
    
    IEnumerator Wait()
    {
        foreach (Transform go in tombolas.transform)
        {
            Animator rotationAnimation = go.GetComponent<Animator>();
            rotationAnimation.SetTrigger("RollTrigger");
            yield return new WaitForSeconds(0.1f);
            
        }
        
        StopAllCoroutines();
    }
}
