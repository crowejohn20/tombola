using TMPro;
using UnityEngine;

public class TumbolaData : MonoBehaviour
{
    public TextDataScriptableObject textValues;
    private string _randomText;
    void Start()
    {
        AssignTextToTumbola();
    }

    private void AssignTextToTumbola()
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<TextMeshProUGUI>() == null) continue;
            _randomText = textValues.text[Random.Range(0, textValues.text.Length)];
            child.GetComponent<TextMeshProUGUI>().text = _randomText;
        }
    }
}
