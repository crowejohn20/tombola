using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TextData", order = 1)]
public class TextDataScriptableObject : ScriptableObject
{
    public string[] text;
}
