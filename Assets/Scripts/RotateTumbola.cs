using UnityEngine;

public class RotateTumbola : MonoBehaviour
{
    private Animator _rotationAnimation;
    public void Start()
    {
        _rotationAnimation = GetComponent<Animator>();
        PlayRotationAnimation();
    }

    public void PlayRotationAnimation()
    {
        _rotationAnimation.SetTrigger("RollTrigger");
    }

    public void StopRotationAnimation()
    {
        //Debug.Log("Stopping");
    }
}
